<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::get ( '/', function () {
	return view ( 'common.index' );
} );

Route::get ( '/career', function () {
	return view ( 'career.index' );
} );

Route::get ( '/soft', function () {
	return view ( 'softsolution.index' );
} );

Route::get ( '/web', function () {
	return view ( 'websolution.index' );
} );

Route::get ( '/digital', function () {
	return view ( 'digitalmarket.index' );
} );

Route::post ( '/sendmail', 'mailcontroller@message')
Route::post ( '/sendmail', 'mailcontroller@message')

Auth::routes ();

Route::get ( '/home', 'HomeController@index' )->name ( 'home' );
