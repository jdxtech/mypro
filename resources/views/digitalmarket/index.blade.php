@extends('layouts.app')
@section('content')
		
		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><b>Digital</b> Marketing</h2>
				<p>Publication of the latest news, articles, and free apps.</p>
			</div>
		</section><!-- //BREADCRUMBS -->
		
		
		<!-- PORTFOLIO -->
		<section id="portfolio">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
					
					<!-- SIDEBAR -->
					<div class="sidebar col-lg-4 col-md-4 pull-right padbot50">
						<!-- TEXT WIDGET -->
						<div class="sidepanel widget_text">
							<div class="single_portfolio_post_title">Rihanna's Costume Designer Comes</div>
							<p>On Tuesday, the daughters of Texas state senator Wendy Davis defended their mom against charges that she�s been smudging her biography to make it look like she was a more involved mom than she actually was. Davis, who is running for governor, has been under fire since a Dallas Morning News article accused her of �blurring� facts, like exactly when she got divorced and how long her family lived in a trailer park.</p>
						</div><!-- //TEXT WIDGET -->
						
						<hr>
						
						<!-- INFO WIDGET -->
						<div class="sidepanel widget_info">
							
							<ul class="shared">
								<li><a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a></li>
								<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
								<li><a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a></li>
								<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
							</ul>
						</div><!-- //INFO WIDGET -->
					</div><!-- //SIDEBAR -->
					
					
					<!-- PORTFOLIO BLOCK -->
					<div class="portfolio_block col-lg-8 col-md-8 pull-left padbot50">
						
						<!-- SINGLE PORTFOLIO POST -->
						<div class="single_portfolio_post clearfix" data-animated="fadeInUp">
							
							<!-- PORTFOLIO SLIDER -->
							<div class="flexslider portfolio_single_slider">
								<ul class="slides">
									<li><img src="images/portfolio/1.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_2.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_3.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_4.jpg" alt="" /></li>
								</ul>
							</div><!-- //PORTFOLIO SLIDER -->
						</div><!-- //SINGLE PORTFOLIO POST -->
					</div><!-- //PORTFOLIO BLOCK -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
			
	
					
			</div>
		</section><!-- //PORTFOLIO -->
	</div><!-- //PAGE -->

	
	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->
@endsection

