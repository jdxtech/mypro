	<meta charset="utf-8">
	<title>ILBS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="shortcut icon" href="images/favicon.ico">
    
	<!-- CSS -->
	<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('css/flexslider.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('css/prettyPhoto.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('css/animate.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ url('css/owl.carousel.css') }}" rel="stylesheet">
	<link href="{{ url('css/style.css') }}" rel="stylesheet" type="text/css" />
    
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500italic,700,500,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">	
    
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if IE]><html class="ie" lang="en"> <![endif]-->
	
	<script src="{{ url('js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/jquery.prettyPhoto.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/jquery.nicescroll.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/superfish.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/jquery.flexslider-min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/owl.carousel.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/animate.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/jquery.BlackAndWhite.js') }}"></script>
	<script src="{{ url('js/myscript.js') }}" type="text/javascript"></script>
	