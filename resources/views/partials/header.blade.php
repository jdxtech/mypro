 
<!-- PRELOADER -->
<img id="preloader" src="images/preloader.gif" alt="" />
<!-- //PRELOADER -->
<div class="preloader_hide">

	<!-- PAGE -->
	<div id="page">
	
		<!-- HEADER -->
		<header>
			
			<!-- MENU BLOCK -->
			<div class="menu_block">
			
				<!-- CONTAINER -->
				<div class="container clearfix">
					
					<!-- LOGO -->
					<div class="logo pull-left">
					<div class="logo">
					<img src="images/logo.jpg" alt="" />
					</div>
					<br>
						<a href="index.html" ><span class="b1" style="margin-left:-45px;">ILBS</span></a>
					</div><!-- //LOGO -->
					
					<!-- SEARCH FORM -->
					
					
					<!-- MENU -->
					<div class="pull-right">
						<nav class="navmenu center">
							<ul>
								<li class="first active scroll_btn"><a href="{{url('/')}}" >Home</a></li>
								<li class="scroll_btn"><a href="{{url('/')}}#about" >About Us</a></li>
								
								<li class="scroll_btn"><a href="{{url('/')}}#news" >Career</a></li>
								<li class="scroll_btn last"><a href="{{url('/')}}#contacts" >Contacts</a></li>
								<li class="sub-menu">
									<a href="javascript:void(0);" >Pages</a>
									<ul>
										<li><a href="{{url('/soft')}}" >Software</a></li>
										<li><a href="{{url('/web')}}" >Web Design</a></li>
										<li><a href="{{url('/digital')}}" >Digital Marketing</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div><!-- //MENU -->
				</div><!-- //MENU BLOCK -->
			</div><!-- //CONTAINER -->
		</header><!-- //HEADER -->
		