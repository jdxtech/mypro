@extends('layouts.app')
@section('content')
		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><b>Web</b> Design</h2>
				<p>Publication of the latest news, articles, and free apps.</p>
			</div>
		</section><!-- //BREADCRUMBS -->
		
		
		<!-- BLOG -->
		<section id="blog">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
				
					<!-- BLOG BLOCK -->
					<div class="blog_block col-lg-9 col-md-9 padbot50">
						
						<!-- SINGLE BLOG POST -->
						<div class="single_blog_post clearfix" data-animated="fadeInUp">
							<div class="single_blog_post_descr">
								
								<div class="single_blog_post_title">Rihanna's Costume Designer Comes Into His Own with New Show</div>
								
							</div>
							<div class="single_blog_post_img"><img src="images/blog/1.jpg" alt="" /></div>
							
							<div class="single_blog_post_content">
								<p class="margbot50">On Tuesday, the daughters of Texas state senator Wendy Davis defended their mom against charges that she�s been smudging her biography to make it look like she was a more involved mom than she actually was. Davis, who is running for governor, has been under fire since a Dallas Morning News article accused her of �blurring� facts, like exactly when she got divorced and how long her family lived in a trailer park. That DMN article also seemingly took her to task for allowing her ex-husband to have parental custody of their daughter, quoting an anonymous source who said, Davis was �not going to let family or raising children or anything else get in her way.�</p>
								<p class="margbot30">I understand why Davis� daughters felt they needed to defend their mom and her parenting. The onslaught on Davis� parenting (and her politics) from the right, particularly in the wake of that DMN article, has been appalling: They�ve called her everything from �Abortion Barbie� to implying she�s a bad, gold digging mom. Her daughter, Dru, points out that her mom always shared joint custody, even though she was living in her father�s house.</p>
								<p class="margbot40">But she also discusses her mom�s involvement in other ways: Her mom was her Brownie troop leader. Her mom was her team�s field hockey mom when Dru was a senior in high school. �She went with me to every field hockey camp, tryout, program that I ever had,� Dru writes, adding, �my sister and I were always her first priority.�</p>
							</div>
							
						</div><!-- //SINGLE BLOG POST -->
						
						
						<!-- SINGLE BLOG POST TAG -->
						
						
						<hr>
						
											
						<hr class="margbot80">
						
						<!-- LEAVE A COMMENT -->
						<div class="leave_comment" data-animated="fadeInUp">
							<h3><b>Leave a</b> Comment</h3>
							
							<form id="comment_form" class="row" action="#" method="post">
								<div class="col-lg-4 col-md-4">
									<input type="text" name="name" value="Your Name *" onFocus="if (this.value == 'Your Name *') this.value = '';" onBlur="if (this.value == '') this.value = 'Your Name *';" />
									<input type="text" name="phone" value="E-mail *" onFocus="if (this.value == 'E-mail *') this.value = '';" onBlur="if (this.value == '') this.value = 'E-mail *';" />
									<input type="text" name="phone" value="Web site" onFocus="if (this.value == 'Web site') this.value = '';" onBlur="if (this.value == '') this.value = 'Web site';" />
									<div class="comment_note">All fields marked with an asterisk (*) are required</div>
								</div>
								<div class="col-lg-8 col-md-8">
									<textarea name="message" onFocus="if (this.value == 'Your message *') this.value = '';" onBlur="if (this.value == '') this.value = 'Your message *';">Your message *</textarea>
									<input class="contact_btn pull-right" type="submit" value="Send message" />
								</div>
							</form>
						</div><!-- //LEAVE A COMMENT -->
					</div><!-- //BLOG BLOCK -->
					
					
					<!-- SIDEBAR -->
					<div class="sidebar col-lg-3 col-md-3 padbot50">
						
						<!-- META WIDGET -->
						<div class="sidepanel widget_meta">
							<ul>
								<li><a href="javascript:void(0);" >Advertising</a></li>
								<li><a href="javascript:void(0);" >Fashion & Trends</a></li>
								<li><a href="javascript:void(0);" >Media Projects</a></li>
								<li><a href="javascript:void(0);" >Small Business</a></li>
								<li><a href="javascript:void(0);" >Creative</a></li>
							</ul>
						</div><!-- //META WIDGET -->
						
						
						<!-- POPULAR POSTS WIDGET -->
						
						
									</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //BLOG -->
	</div><!-- //PAGE -->

	
	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->
@endsection

