@extends('layouts.app')
@section('content')
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><b>Software </b> Development</h2>
				<p>Publication of the latest news, articles, and free apps.</p>
			</div>
		</section><!-- //BREADCRUMBS -->
		
		
		<!-- BLOG -->
		<section id="blog">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
				
					<!-- BLOG BLOCK -->
					<div class="blog_block col-lg-9 col-md-9 padbot50">
						
						<!-- BLOG POST -->
						<div class="blog_post margbot50 clearfix" data-animated="fadeInUp">
							<div class="blog_post_img">
								<img src="images/blog/1.jpg" alt="" />
								<a class="zoom" href="blog-post.html" ></a>
							</div>
							<div class="blog_post_descr">
								<a class="blog_post_title" href="blog-post.html" >Rihanna's Costume Designer Comes Into His Own with New Show</a>
								
								<hr>
								<div class="blog_post_content">The best way to describe Adam Selman would be simply to say that he is the epitome of cool. So it�s only fitting that his first runway collection was made for the ultimate cool girl.</div>
								
							</div>
						</div><!-- //BLOG POST -->
						

						<!-- BLOG POST -->
						<div class="blog_post margbot50 clearfix" data-animated="fadeInUp">
							<div class="blog_post_img">
								<img src="images/blog/2.jpg" alt="" />
								<a class="zoom" href="blog-post.html" ></a>
							</div>
							<div class="blog_post_descr">
								
								<a class="blog_post_title" href="blog-post.html" >The Four Week Workout to Tank Top Worthy Arm</a>
								
								<hr>
								<div class="blog_post_content">Our stomachs, legs, and butts, (thanks to Jen Selter) are usually top priority areas when it comes to getting in shape. But when I'm tagged in a photo on Instagram, I quite often find myself wishing for an arm-sculpting filter.</div>
								
							</div>
						</div><!-- //BLOG POST -->
						
						
						<!-- BLOG POST -->
						<div class="blog_post margbot50 clearfix" data-animated="fadeInUp">
							<div class="blog_post_img">
								<img src="images/blog/3.jpg" alt="" />
								<a class="zoom" href="blog-post.html" ></a>
							</div>
							<div class="blog_post_descr">
								
								<a class="blog_post_title" href="blog-post.html" >The Fault in Our Stars Trailer Makes Us Feel All The Feelings</a>
								<ul class="blog_post_info">
									<li><a href="javascript:void(0);" >Admin</a></li>
									<li><a href="javascript:void(0);" >Creative</a></li>
									<li><a href="javascript:void(0);" >3 Comments</a></li>
								</ul>
								<hr>
								<div class="blog_post_content">The trailer for 'The Fault in Our Stars', the movie based on John Green's tear-jerky YA novel, is finally out, and it's good.</div>
								
							</div>
						</div><!-- //BLOG POST -->
						
						
																																				
						
						
					</div><!-- //BLOG BLOCK -->
					
					
					<!-- SIDEBAR -->
					<div class="sidebar col-lg-3 col-md-3 padbot50">
						
						<!-- META WIDGET -->
						<div class="sidepanel widget_meta">
							<ul>
								<li><a href="javascript:void(0);" >Advertising</a></li>
								<li><a href="javascript:void(0);" >Fashion & Trends</a></li>
								<li><a href="javascript:void(0);" >Media Projects</a></li>
								<li><a href="javascript:void(0);" >Small Business</a></li>
								<li><a href="javascript:void(0);" >Creative</a></li>
							</ul>
						</div><!-- //META WIDGET -->
						
						
						<!-- POPULAR POSTS WIDGET -->
						
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //BLOG -->
	</div><!-- //PAGE -->

	
	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->
@endsection

