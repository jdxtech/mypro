@extends('layouts.app')

@section('content')
   <!-- PRELOADER -->
<img id="preloader" src="images/preloader.gif" alt="" />
<!-- //PRELOADER -->
<div class="preloader_hide">

	<!-- PAGE -->
	<div id="page" class="single_page">
	
		<!-- HEADER -->
		<header>
			
			<!-- MENU BLOCK -->
			<div class="menu_block">
			
				<!-- CONTAINER -->
				<div class="container clearfix">
					
					<!-- LOGO -->
					<div class="logo pull-left">
						<a href="index.html" ><span class="b1">INFOLIVE</span><span class="b2">BUSINESS SERVICE</span></a>
					</div><!-- //LOGO -->
					
					<!-- SEARCH FORM -->
					
					
					<!-- MENU -->
					<div class="pull-right">
						<nav class="navmenu center">
							<ul>
								<li class="first scroll_btn"><a href="index.html">Home</a></li>
								<li class="scroll_btn"><a href="index.html#about">About Us</a></li>
								
								<li class="scroll_btn"><a href="index.html#news">Career</a></li>
								<li class="scroll_btn last"><a href="index.html#contacts">Contacts</a></li>
								<li class="sub-menu active">
									<a href="javascript:void(0);">Pages</a>
									<ul>
										<li><a href="Software.html">Software</a></li>
										<li class="active"><a href="Web Design.html">Web Design</a></li>
										<li><a href="Digital Marketing.html">Digital Marketing</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div><!-- //MENU -->
				</div><!-- //MENU BLOCK -->
			</div><!-- //CONTAINER -->
		</header><!-- //HEADER -->
		
		
		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><b>Career</b> </h2>
				<p>Publication of the latest news, articles, and free apps.</p>
			</div>
		</section><!-- //BREADCRUMBS -->
		
		
		<!-- BLOG -->
		<section id="blog">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
				
					<!-- BLOG BLOCK -->
					<div class="blog_block col-lg-9 col-md-9 padbot50">
						
						<!-- SINGLE BLOG POST -->
						<div class="single_blog_post clearfix" data-animated="fadeInUp">
							<div class="single_blog_post_descr">
								
								<div class="single_blog_post_title">Jobs</div>
								
							</div>
							<div class="single_blog_post_img"><img src="images/blog/1.jpg" alt="" /></div>
							
							<div class="single_blog_post_content">
								<p class="margbot50">On Tuesday, the daughters of Texas state senator Wendy Davis defended their mom against charges that she�s been smudging her biography to make it look like she was a more involved mom than she actually was. Davis, who is running for governor, has been under fire since a Dallas Morning News article accused her of �blurring� facts, like exactly when she got divorced and how long her family lived in a trailer park. That DMN article also seemingly took her to task for allowing her ex-husband to have parental custody of their daughter, quoting an anonymous source who said, Davis was �not going to let family or raising children or anything else get in her way.�</p>
								<p class="margbot30">I understand why Davis� daughters felt they needed to defend their mom and her parenting. The onslaught on Davis� parenting (and her politics) from the right, particularly in the wake of that DMN article, has been appalling: They�ve called her everything from �Abortion Barbie� to implying she�s a bad, gold digging mom. Her daughter, Dru, points out that her mom always shared joint custody, even though she was living in her father�s house.</p>
								<p class="margbot40">But she also discusses her mom�s involvement in other ways: Her mom was her Brownie troop leader. Her mom was her team�s field hockey mom when Dru was a senior in high school. �She went with me to every field hockey camp, tryout, program that I ever had,� Dru writes, adding, �my sister and I were always her first priority.�</p>
							</div>
							
						</div><!-- //SINGLE BLOG POST -->
						
						
						<!-- SINGLE BLOG POST TAG -->
						
						
						<hr>
						
						<!-- COMMENTS -->
						<div id="comments" class="margbot30" data-animated="fadeInUp">
							<h3><b>Jobs Details </b><span class="comments_count">(152)</span></h3>
							
							<ul>
							   <li class="clearfix" data-animated="fadeInUp">
									<div class="pull-left avatar">
										<a href="javascript:void(0);" ><img src="images/avatar1.jpg" alt="" /></a>
									</div>
									<div class="comment_right">
										<div class="comment_info clearfix">
											<div class="pull-left comment_author">Stanislav Kerimov</div>
											<div class="pull-left comment_inf_sep">|</div>
											<div class="pull-left comment_date">13 January 2014</div>
										</div>
										<p>Thank you so much for putting this together Jeremy. Most of these seem like common sense but it is amazing how many times I see new employees having the worst days of their life because managers/leaders don�t want to be �bothered� with the new guy.</p>
									</div>
								</li>
								<li class="clearfix" data-animated="fadeInUp">
									<div class="pull-left avatar">
										<a href="javascript:void(0);" ><img src="images/avatar2.jpg" alt="" /></a>
									</div>
									<div class="comment_right">
										<div class="comment_info clearfix">
											<div class="pull-left comment_author">Anna Balashova</div>
											<div class="pull-left comment_inf_sep">|</div>
											<div class="pull-left comment_date">10 January 2014</div>
										</div>
										<p>I would add under �keep the busy� to make sure that every team member is aware of the new team member starting, and even thought the first 1-2 days may be meet and greats, to have them up with access to everything they need to preform their tasks.</p>
									</div>
								</li>
							</ul>
						</div>
						<!-- //COMMENTS -->
						
						<hr class="margbot80">
						
						<!-- LEAVE A COMMENT -->
						<div class="leave_comment" data-animated="fadeInUp">
							<h3><b>Apply</b> Now</h3>
							
							<form id="comment_form" class="row" action="#" method="post">
								<div class="col-lg-4 col-md-4">
									<input type="text" name="name" value="Your Name *" onFocus="if (this.value == 'Your Name *') this.value = '';" onBlur="if (this.value == '') this.value = 'Your Name *';" />
									<input type="text" name="phone" value="E-mail *" onFocus="if (this.value == 'E-mail *') this.value = '';" onBlur="if (this.value == '') this.value = 'E-mail *';" />
									<input type="text" name="phone" value="Web site" onFocus="if (this.value == 'Web site') this.value = '';" onBlur="if (this.value == '') this.value = 'Web site';" />
									<div class="comment_note">All fields marked with an asterisk (*) are required</div>
								</div>
								<div class="col-lg-8 col-md-8">
									<textarea name="message" onFocus="if (this.value == 'Your message *') this.value = '';" onBlur="if (this.value == '') this.value = 'Your message *';">Your message *</textarea>
									<input class="contact_btn pull-right" type="submit" value="Send message" />
								</div>
							</form>
						</div><!-- //LEAVE A COMMENT -->
					</div><!-- //BLOG BLOCK -->
					
					
					<!-- SIDEBAR -->
					<div class="sidebar col-lg-3 col-md-3 padbot50">
						
						<!-- META WIDGET -->
						<div class="sidepanel widget_meta">
							<ul>
								<li><a href="javascript:void(0);" >Advertising</a></li>
								<li><a href="javascript:void(0);" >Fashion & Trends</a></li>
								<li><a href="javascript:void(0);" >Media Projects</a></li>
								<li><a href="javascript:void(0);" >Small Business</a></li>
								<li><a href="javascript:void(0);" >Creative</a></li>
							</ul>
						</div><!-- //META WIDGET -->
						
						
						<!-- POPULAR POSTS WIDGET -->
						
						
						<hr>
						
										</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //BLOG -->
	</div><!-- //PAGE -->

	
	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->

	
	<!-- FOOTER -->
	<footer>
			
		<!-- CONTAINER -->
		<div class="container">
			
			<!-- ROW -->
			<div class="row" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<div class="col-lg-4 col-md-4 col-sm-6 padbot30">
					<h4><b>Featured</b> posts</h4>
					<div class="recent_posts_small clearfix">
						<div class="post_item_img_small">
							<img src="images/blog/1.jpg" alt="" />
						</div>
						<div class="post_item_content_small">
							<a class="title" href="blog.html" >As we have developed a unique layout template</a>
							<ul class="post_item_inf_small">
								<li>10 January 2014</li>
							</ul>
						</div>
					</div>
					<div class="recent_posts_small clearfix">
						<div class="post_item_img_small">
							<img src="images/blog/2.jpg" alt="" />
						</div>
						<div class="post_item_content_small">
							<a class="title" href="blog.html" >How much is to develop a design for the game?</a>
							<ul class="post_item_inf_small">
								<li>14 January 2014</li>
							</ul>
						</div>
					</div>
					<div class="recent_posts_small clearfix">
						<div class="post_item_img_small">
							<img src="images/blog/3.jpg" alt="" />
						</div>
						<div class="post_item_content_small">
							<a class="title" href="blog.html" >How to pump designer</a>
							<ul class="post_item_inf_small">
								<li>21 December 2013</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-6 padbot30 foot_about_block">
					<h4><b>About</b> us</h4>
					<p>We value people over profits, quality over quantity, and keeping it real. As such, we deliver an unmatched working relationship with our clients.</p>
					<p>Our team is intentionally small, eclectic, and skilled; with our in-house expertise, we provide sharp and</p>
					<ul class="social">
						<li><a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a></li>
						<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
						<li><a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a></li>
						<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
						<li><a href="javascript:void(0);" ><i class="map_show fa fa-map-marker"></i></a></li>
					</ul>
				</div>
				
				<div class="respond_clear"></div>
				
				<div class="col-lg-4 col-md-4 padbot30">
					<h4><b>Contacts</b> Us</h4>
					
					<!-- CONTACT FORM -->
					<div class="span9 contact_form">
						<div id="note"></div>
						<div id="fields">
							<form id="contact-form-face" class="clearfix" action="#">
								<input type="text" name="name" value="Name" onFocus="if (this.value == 'Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Name';" />
								<textarea name="message" onFocus="if (this.value == 'Message') this.value = '';" onBlur="if (this.value == '') this.value = 'Message';">Message</textarea>
								<input class="contact_btn" type="submit" value="Send message" />
							</form>
						</div>
					</div><!-- //CONTACT FORM -->
				</div>
			</div><!-- //ROW -->
			<div class="row copyright">
				<div class="col-lg-12 text-center">
				
				 <p>Crafted with <i class="fa fa-heart"></i>, <a href="http://designscrazed.org/" >Designscrazed</a></p>
				</div>
			
			</div><!-- //ROW -->
	</footer><!-- //FOOTER -->
@endsection

